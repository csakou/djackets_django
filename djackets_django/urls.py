from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

API_V1 = 'api/v1/'

urlpatterns = [
    path("admin/", admin.site.urls),
    path(API_V1, include('djoser.urls')),
    path(API_V1, include('djoser.urls.authtoken')),
    path(API_V1, include('product.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
